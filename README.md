# Running Demo

Run `npm start`

Open https://localhost:3000

Open [Bitsocket](https://txo.bitsocket.network/socket/ewogICJ2IjogMywKICAicSI6IHsKICAgICJmaW5kIjogewogICAgICAib3V0LnMxIjogIjE5SHhpZ1Y0UXlCdjN0SHBRVmNVRVF5cTFwelpWZG9BdXQiCiAgICB9CiAgfSwKICAiciI6IHsKICAgICJmIjogIlsgLltdIHwge3R4OiAudHguaCwgc3RyaW5nOiAub3V0WzBdLnMyfSBdIgogIH0KfQ==#)

```json
{
  "v": 3,
  "q": {
    "find": {
      "out.s1": "19HxigV4QyBv3tHpQVcUEQyq1pzZVdoAut"
    }
  },
  "r": {
    "f": "[ .[] | {tx: .tx.h, string: .out[0].s2} ]"
  }
}
```

Enter a message, then submit with moneybutton.
