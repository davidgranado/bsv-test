const express = require('express');
const { join } = require('path');

const PORT = 3000;
const app = express();

app.get('/', (req, res) => {
	res.sendFile(join(__dirname + '/index.html'));
})

app.listen(PORT, () => {
	console.log(`Example app listening at http://localhost:${PORT}`)
})